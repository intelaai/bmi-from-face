#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""DNN BMI Estimation."""

import os
import pickle

import numpy as np
import tensorflow as tf
from sklearn import metrics

ROOT = os.path.dirname(os.path.abspath(__file__))
DATA_ROOT = os.path.abspath(os.path.join(ROOT, '../../data/'))
PROCESSED_ROOT = os.path.join(DATA_ROOT, 'processed')


def load_data(name, bmi=False):
    """Helper function to load data."""
    X_path = os.path.join(PROCESSED_ROOT, f'face_bmi_X_{name}.pkl')
    with open(X_path, 'rb') as f:
        X = pickle.load(f)
    if bmi:
        y_path = os.path.join(PROCESSED_ROOT, f'face_bmi_y_bmi_{name}.pkl')
    else:
        y_path = os.path.join(PROCESSED_ROOT, f'face_bmi_y_{name}.pkl')
    with open(y_path, 'rb') as f:
        y = pickle.load(f)
    return X, y


def digitize(arr):
    """Helper function to bin bmi estimation."""
    return np.digitize(arr, bins=[18.5, 25., 30])


X_train, y_train = load_data('train', bmi=True)
X_test, y_test = load_data('test', bmi=True)
X_all = np.concatenate((X_train, X_test))
y_all = np.concatenate((y_train, y_test))

input_shape = [X_train.shape[1], ]
model = tf.keras.Sequential([
    tf.keras.layers.Dense(512, activation='relu', input_shape=input_shape),
    tf.keras.layers.Dense(512, activation='relu'),
    tf.keras.layers.Dense(512, activation='relu'),
    tf.keras.layers.Dense(1, activation='linear')])
# tf.keras.layers.Dense(4, activation='linear')])

checkpoint_path = '../models/dnn-reg/checkpoint.ckpt'
checkpoint_dir = os.path.dirname(checkpoint_path)

# Create a callback that saves the model's weights
cp = []
save = False
if save:
    cp = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                            save_weights_only=True, verbose=1)

# optimizer = tf.keras.optimizers.Adam(learning_rate=1e-3)
optimizer = tf.keras.optimizers.Adagrad(learning_rate=1e-3)
model.compile(loss='mse', optimizer=optimizer)
# loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
# model.compile(loss=loss, optimizer=optimizer)

model.fit(X_train, y_train, epochs=50, batch_size=32, callbacks=[], verbose=True)

# preds_train = np.argmax(model.predict(X_train), axis=1)
# preds_test = np.argmax(model.predict(X_test), axis=1)

preds_train = model.predict(X_train)
preds_test = model.predict(X_test)

# metrics
for dset, labels, pred in [('train', y_train, preds_train),
                           ('test', y_test, preds_test)]:
    print(dset)
    for metric in [metrics.max_error, metrics.mean_absolute_error]:
        print(metric.__name__, metric(labels, pred))

preds_train = digitize(preds_train)
preds_test = digitize(preds_test)
y_train = digitize(y_train)
y_test = digitize(y_test)

# metrics
for dset, labels, pred in [('train', y_train, preds_train),
                           ('test', y_test, preds_test)]:
    for metric in [metrics.accuracy_score, metrics.confusion_matrix]:
        print(dset)
        print(metric.__name__, '\n', metric(labels, pred))
