import os
import pickle

from PIL import Image
import numpy as np
from sklearn.svm import SVR
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn import metrics
from joblib import dump

ROOT = os.path.dirname(os.path.abspath(__file__))
DATA_ROOT = os.path.abspath(os.path.join(ROOT, '../../data/'))
MODEL_ROOT = os.path.abspath(os.path.join(ROOT, '../../models/'))
IMG_ROOT = os.path.join(DATA_ROOT, 'raw/images')
ALIGNED_ROOT = os.path.join(DATA_ROOT, 'interim/face_aligned')
PROCESSED_ROOT = os.path.join(DATA_ROOT, 'processed')


def load_data(name, bmi=False):
    X_path = os.path.join(PROCESSED_ROOT, f'face_bmi_X_{name}.pkl')
    with open(X_path, 'rb') as f:
        X = pickle.load(f)
    if bmi:
        y_path = os.path.join(PROCESSED_ROOT, f'face_bmi_y_{name}.pkl')
    else:
        y_path = os.path.join(PROCESSED_ROOT, f'face_bmi_y_{name}.pkl')
    with open(y_path, 'rb') as f:
        y = pickle.load(f)
    return X, y


def digitize(arr):
    return np.digitize(arr, bins=[18.5, 25., 30])


X_train, y_train = load_data('train', bmi=True)
X_test, y_test = load_data('test', bmi=True)
X_all = np.concatenate((X_train, X_test))
y_all = np.concatenate((y_train, y_test))

C = 9
eps = .75
# clf = make_pipeline(StandardScaler(), SVR(C=C))
# clf.fit(X_train, y_train)
# clf.fit(X_all, y_all)
clf = SVR(C=C, epsilon=eps)
clf.fit(X_all, y_all)

dumppath = os.path.join(MODEL_ROOT, f'svm/final_svr.joblib')
dump(clf, dumppath)

preds_train = clf.predict(X_train)
preds_test = clf.predict(X_test)

# metrics
for dset, labels, pred in [('train', y_train, preds_train),
                           ('test', y_test, preds_test)]:
    print(dset)
    for metric in [metrics.max_error, metrics.mean_absolute_error]:
        print(metric.__name__, metric(labels, pred))

preds_train = digitize(preds_train)
preds_test = digitize(preds_test)
y_train = digitize(y_train)
y_test = digitize(y_test)

# metrics
for dset, labels, pred in [('train', y_train, preds_train),
                           ('test', y_test, preds_test)]:
    for metric in [metrics.accuracy_score, metrics.confusion_matrix]:
        print(dset)
        print(metric.__name__, '\n', metric(labels, pred))


investigate = False
if investigate:
    with open(os.path.join(PROCESSED_ROOT, 'face_bmi.pkl'), 'rb') as f:
        data = pickle.load(f)

    for i, source in enumerate(data['index']):
        pred = clf.predict([data['features'][i]])
        if data['cat'][i] == 3:
            print(source)
            print(pred, digitize(pred))
            print(data['bmi'][i], data['cat'][i])
            yn = input('Show picture? [y/N]')
            if yn == 'y':
                img = Image.open(os.path.join(IMG_ROOT, source))
                img.show()
                face = Image.open(os.path.join(ALIGNED_ROOT, source))
                face.show()
            yn = input('Continue? [Y/n]')
            if yn == 'n':
                break
