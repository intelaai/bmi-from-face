#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Runkicker datasets.
Image -> Face -> Preprocessing -> Features
BMI -> BMI, Categories
"""
import cv2
import os
import pickle

from tqdm import tqdm

from PIL import Image
from sklearn.model_selection import train_test_split
import numpy as np
import pandas as pd

import torch
import torchvision

from facenet_pytorch import MTCNN, InceptionResnetV1

ROOT = os.path.dirname(os.path.abspath(__file__))
DATA_ROOT = os.path.abspath(os.path.join(ROOT, '../../data/'))
IMG_ROOT = os.path.join(DATA_ROOT, 'raw/images/face')
INTERIM_ROOT = os.path.join(DATA_ROOT, 'interim')
PROCESSED_ROOT = os.path.join(DATA_ROOT, 'processed')


def step1():
    """Load raw data, remove entries without associated image,
    categorize bmi values and saves interim data to face_bmi.pkl
    """
    raw = pd.read_csv(os.path.join(DATA_ROOT, 'raw/train.csv'),
                      # usecols=['source', 'bmi'])
                      usecols=['index', 'weight'])
    face_bmi = raw.copy()

    # remove entries with no associated image
    for idx in raw.index:
        if not os.path.isfile(f'{IMG_ROOT}/{raw["index"][idx]}'):
            face_bmi.drop(index=idx, inplace=True)

    face_bmi.to_csv(os.path.join(INTERIM_ROOT, 'face_weight.csv'))
    face_bmi.to_pickle(os.path.join(INTERIM_ROOT, 'face_weight.pkl'))


def step2():
    """Crop all faces from raw images.
    Saves interim data to interim/face_aligned dir.
    """
    mtcnn = MTCNN()

    with open(os.path.join(INTERIM_ROOT, 'face_bmi.pkl'), 'rb') as f:
        data = pickle.load(f)

    for src in tqdm(data["index"]):
        full_src = os.path.join(IMG_ROOT, src)
        save_src = os.path.join(INTERIM_ROOT, 'face_aligned', src)
        face_img = np.array(Image.open(full_src))
        face_img = np.array(mtcnn(face_img))
        print(face_img.shape)
        face_img = Image.fromarray(np.array(face_img))
        print(face_img.shape)
        assert 1 == 2


def step3():
    """Extract VGGFace2 features for aligned face images.
    Saves interim data to interim/features dir.
    """
    vggface2 = InceptionResnetV1(pretrained='vggface2').eval()

    with open(os.path.join(INTERIM_ROOT, 'face_bmi.pkl'), 'rb') as f:
        data = pickle.load(f)

    for src in tqdm(data["index"]):
        try:
            img = Image.open(os.path.join(INTERIM_ROOT, 'face_aligned', src))
        except FileNotFoundError:
            print(f'File {src} not found.')
        else:
            img = torchvision.transforms.functional.to_tensor(img).unsqueeze(0)
            features = vggface2(img)
            pkl_src = os.path.splitext(src)[0] + '.pkl'
            with open(os.path.join(INTERIM_ROOT, 'features', pkl_src), 'wb') as f:
                pickle.dump(features.detach().numpy(), f)


def step4():
    """Add features to dataset."""
    def load_features(source):
        pkl_src = os.path.splitext(source)[0]+'.pkl'
        full_source = os.path.join(INTERIM_ROOT, 'features', pkl_src)
        with open(full_source, 'rb') as f:
            features = pickle.load(f)
        return features

    with open(os.path.join(INTERIM_ROOT, 'face_bmi.pkl'), 'rb') as f:
        data = pickle.load(f)

    num_samples = data.shape[0]
    feature_length = 512

    dataset = {'index': np.empty((num_samples,), dtype=object),
               'features': np.empty((num_samples, feature_length)),
               'bmi': np.empty((num_samples,))}
    rows = [row for row in data[['index', 'bmi']].values]
    for i, (source, bmi) in enumerate(rows):
        try:
            dataset['features'][i] = load_features(source)
            dataset['index'][i] = source
            dataset['bmi'][i] = bmi
        except FileNotFoundError:
            print(f'File {source} not found.')
    with open(os.path.join(PROCESSED_ROOT, 'face_bmi.pkl'), 'wb') as f:
        print('Here')
        pickle.dump(dataset, f)


def step5():
    """Split train and test set."""
    random_seed = 0

    with open(os.path.join(PROCESSED_ROOT, 'face_bmi.pkl'), 'rb') as f:
        data = pickle.load(f)
    X_train, X_test, y_train, y_test = train_test_split(
        data['features'], data['bmi'], test_size=0.1,
        random_state=random_seed)
    with open(os.path.join(PROCESSED_ROOT, 'face_bmi_X_train.pkl'), 'wb') as f:
        pickle.dump(X_train, f)
    with open(os.path.join(PROCESSED_ROOT, 'face_bmi_y_train.pkl'), 'wb') as f:
        pickle.dump(y_train, f)
    with open(os.path.join(PROCESSED_ROOT, 'face_bmi_X_test.pkl'), 'wb') as f:
        pickle.dump(X_test, f)
    with open(os.path.join(PROCESSED_ROOT, 'face_bmi_y_test.pkl'), 'wb') as f:
        pickle.dump(y_test, f)


if __name__ == '__main__':
    # step1()
    # step2()
    # step3()
    step4()
    step5()
