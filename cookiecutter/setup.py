from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='BMI prediction from portrait photographs.',
    author='Arcanum AI',
    license='',
)
